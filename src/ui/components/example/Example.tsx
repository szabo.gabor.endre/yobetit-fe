import React from 'react';

import exampleStyles from './example.module.scss';

export interface ExampleProps {
  children: string;
  onClick: () => void;
}

/**
 * General component description.
 */
export const Example: React.FC<ExampleProps> = ({
  children = 'example',
  onClick,
}: ExampleProps) => {
  return (
    <button type="button" onClick={onClick} className={exampleStyles.example}>
      {children}
    </button>
  );
};
