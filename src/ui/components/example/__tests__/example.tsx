import { render } from '@testing-library/react';
import React from 'react';

import { Basic } from '../stories';

describe('UI | Components | Example', () => {
  it('Test it', () => {
    expect(true).toBeTruthy();
  });

  it('Snapshot Basic', () => {
    const { asFragment } = render(<Basic />);

    expect(asFragment()).toMatchSnapshot();
  });
});
