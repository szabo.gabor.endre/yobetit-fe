import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import { initReactI18next } from 'react-i18next';

import { defaultLanguage, languages } from 'app/config/languages';
import { format } from 'app/utils/i18n/format';

i18n
  .use(Backend)
  .use(initReactI18next)
  .init({
    lng: defaultLanguage,
    fallbackLng: defaultLanguage,
    supportedLngs: languages,
    fallbackNS: 'common',
    backend: {
      loadPath: '{{lng}}/{{ns}}',
      parse: (data: string) => data,
      ajax: (
        url: string,
        _options: unknown,
        callback: (response: string, xhr: { [key: string]: string | number }) => void,
      ) => {
        import(`app/translations/${url}.json`)
          .then(locale => {
            callback(locale, { status: 200 });
          })
          .catch(() => {
            callback('', { status: 404 });
          });
      },
    },
    interpolation: {
      format,
      escapeValue: false,
    },
  });

export default i18n;
