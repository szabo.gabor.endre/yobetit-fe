import { format as dateFormat } from 'date-fns';

import { locales } from 'app/config/date';
import { Languages, defaultLanguage } from 'app/config/languages';

export const format = (
  value: never,
  formatKey: string,
  lng: Languages = defaultLanguage,
): string => {
  switch (formatKey) {
    case 'dateLong':
      return dateFormat(value, 'PPP', { locale: locales[lng] });
    default:
      return value;
  }
};
