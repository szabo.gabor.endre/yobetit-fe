import type { AjaxResponse as RxjsAjaxResponse } from 'rxjs/ajax';

export interface AjaxResponse<T> extends RxjsAjaxResponse {
  response: T;
}
