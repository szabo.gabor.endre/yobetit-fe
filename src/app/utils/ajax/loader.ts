import { AjaxResponse } from './ajaxResponse';

enum Status {
  Idle = 'idle',
  Pending = 'pending',
  Failure = 'failure',
  Success = 'success',
}

const loader = <V, E = string>(
  status: Status,
  value?: V,
  error?: E,
  statusCode = 0,
): Loader<V, E> => ({
  status,
  value,
  error,
  statusCode,
});

export interface Loader<V, E = string> {
  status: Status;
  statusCode: number;
  value?: V;
  error?: E;
}

export const failure = <V, E>(error?: E, value?: V, statusCode = 0): Loader<V, E> =>
  loader(Status.Failure, value, error, statusCode);
export const isFailure = <V, E>(model: Loader<V, E>): boolean => model.status === Status.Failure;

export const idle = <V>(value?: V): Loader<V> => loader(Status.Idle, value);
export const isIdle = <V>(model: Loader<V>): boolean => model.status === Status.Idle;

export const pending = <V>(value?: V): Loader<V> => loader(Status.Pending, value);
export const isPending = <V>(model: Loader<V>): boolean => model.status === Status.Pending;

export const success = <V>(response: AjaxResponse<V>): Loader<V, undefined> =>
  loader(Status.Success, response.response, undefined, response.status);
export const isSuccess = <V>(model: Loader<V>): boolean => model.status === Status.Success;
