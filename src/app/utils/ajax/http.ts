import type { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';

import type { AjaxResponse } from './ajaxResponse';

const systemBaseUrl = process.env.API_URL;

export const http = {
  get: <R>(
    url: string,
    headers?: Record<string, unknown>,
    baseUrl: string = systemBaseUrl,
  ): Observable<AjaxResponse<R>> => ajax.get(`${baseUrl}${url}`, headers),
  post: <R, B>(
    url: string,
    body?: B,
    headers?: Record<string, unknown>,
    baseUrl: string = systemBaseUrl,
  ): Observable<AjaxResponse<R>> => ajax.post(`${baseUrl}${url}`, body, headers),
  put: <R, B>(
    url: string,
    body?: B,
    headers?: Record<string, unknown>,
    baseUrl: string = systemBaseUrl,
  ): Observable<AjaxResponse<R>> => ajax.put(`${baseUrl}${url}`, body, headers),
  delete: <R>(
    url: string,
    headers?: Record<string, unknown>,
    baseUrl: string = systemBaseUrl,
  ): Observable<AjaxResponse<R>> => ajax.delete(`${baseUrl}${url}`, headers),
};
