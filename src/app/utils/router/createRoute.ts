import React from 'react';

import { Path } from './createPath';
import type { Route } from './route';

export const createRoute = (
  id: string,
  path: Path,
  component: React.LazyExoticComponent<React.ComponentType>,
  exact = true,
  strict = true,
  sensitive = true,
): Route => ({
  path: Object.values(path),
  id,
  component,
  exact,
  strict,
  sensitive,
});
