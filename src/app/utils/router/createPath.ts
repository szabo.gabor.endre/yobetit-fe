import { Languages } from 'app/config/languages';

export type Path = Record<Languages, string>;

export const createPath = (path: Path): Path => path;
