import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import { Path } from './createPath';
import { createUrl, Params } from './createUrl';

export const useCreateUrl = (): ((
  path: Path,
  params?: Record<string, string | number | boolean | undefined> | undefined,
) => string) => {
  const { i18n } = useTranslation();

  const func = useCallback(
    (path: Path, params?: Params) => createUrl(i18n.language, path, params),
    [i18n.language],
  );

  return func;
};
