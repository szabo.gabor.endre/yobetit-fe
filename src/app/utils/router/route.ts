import React from 'react';

export interface Route {
  id: string;
  component: React.LazyExoticComponent<React.ComponentType>;
  path: string[];
  exact: boolean;
  strict: boolean;
  sensitive: boolean;
}
