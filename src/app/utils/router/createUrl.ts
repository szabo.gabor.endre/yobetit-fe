import { generatePath } from 'react-router';

import { Languages } from 'app/config/languages';

import { Path } from './createPath';

export type Params = Record<string, string | number | boolean | undefined>;

export const createUrl = (currentLng: string | Languages, path: Path, params?: Params): string => {
  return generatePath(path[currentLng as Languages], params);
};
