import { useSelector } from 'react-redux';

import { isPending } from '../ajax/loader';
import { UserData } from 'app/services/api/auth/me';
import { meRequestSelector, isLoggedInSelector, userDataSelector } from 'app/store/auth/selectors';

export const useAuth = (): {
  isLoggedIn: boolean;
  userData?: UserData;
  isLoading: boolean;
} => {
  const meRequest = useSelector(meRequestSelector);
  const isLoggedIn = useSelector(isLoggedInSelector);
  const userData = useSelector(userDataSelector);

  return {
    isLoggedIn,
    userData,
    isLoading: isPending(meRequest),
  };
};
