import { LinkProps as RouterLinkProps } from 'react-router-dom';

import { Path } from 'app/utils/router/createPath';
import { Params } from 'app/utils/router/createUrl';

type AllowedRouterLinkProps = 'replace';

export interface LinkProps extends Pick<RouterLinkProps, AllowedRouterLinkProps> {
  path: Path;
  params?: Params;
}

export type Props = LinkProps;
