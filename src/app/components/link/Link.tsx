import React, { useMemo } from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { useCreateUrl } from 'app/utils/router/useCreateUrl';

import type { Props } from './interfaces';

export const Link: React.FC<Props> = ({ path, params, children, replace }) => {
  const createUrl = useCreateUrl();
  const to = useMemo(() => createUrl(path, params), [createUrl, path, params]);

  return (
    <RouterLink to={to} replace={replace}>
      {children}
    </RouterLink>
  );
};
