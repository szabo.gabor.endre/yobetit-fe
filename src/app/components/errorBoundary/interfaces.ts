import { WithTranslation } from 'react-i18next';

export interface ErrorBoundaryState {
  hasError: boolean;
}

export type Props = WithTranslation;
export type State = ErrorBoundaryState;
