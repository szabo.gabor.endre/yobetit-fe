import React from 'react';
import { withTranslation } from 'react-i18next';

import type { Props, State } from './interfaces';

class ErrorBoundaryComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  static getDerivedStateFromError() {
    return {
      hasError: true,
    };
  }

  render() {
    const { children, t } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return t('common:somethingWentWrong');
    }

    return children;
  }
}

export const ErrorBoundary = withTranslation(['common'])(ErrorBoundaryComponent);
