import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import { languages } from 'app/config/languages';

const LanguageSelectorComponent: React.FC = () => {
  const [t, i18n] = useTranslation(['languages']);

  const changeCurrentLanguage = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      i18n.changeLanguage(e.currentTarget.value);
    },
    [i18n],
  );

  return (
    <select onChange={changeCurrentLanguage} value={i18n.language}>
      {languages.map(language => (
        <option key={language} value={language}>
          {t(`languages:${language}`)}
        </option>
      ))}
    </select>
  );
};

export const LanguageSelector = React.memo(LanguageSelectorComponent);
