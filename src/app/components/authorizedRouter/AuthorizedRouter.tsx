import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { AuthorizedLayout } from 'app/components/layouts/authorizedLayout/AuthorizedLayout';
import { authorizedRoutes, noMatchRoute } from 'app/routes';
import { paths } from 'app/routes/paths';
import { useAuth } from 'app/utils/auth/useAuth';
import { useCreateUrl } from 'app/utils/router/useCreateUrl';

const AuthorizedRouterComponent: React.FC = () => {
  const createUrl = useCreateUrl();
  const { isLoggedIn } = useAuth();

  if (!isLoggedIn) {
    return <Redirect to={createUrl(paths.login)} />;
  }

  return (
    <AuthorizedLayout>
      <Switch>
        {authorizedRoutes.map(route => (
          <Route
            key={route.id}
            path={route.path}
            component={route.component}
            exact={route.exact}
            strict={route.strict}
            sensitive={route.sensitive}
          />
        ))}
        <Route component={noMatchRoute.component} />
      </Switch>
    </AuthorizedLayout>
  );
};

export const AuthorizedRouter = React.memo(AuthorizedRouterComponent);
export default AuthorizedRouter;
