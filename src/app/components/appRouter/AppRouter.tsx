import React, { Suspense, useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { Loading } from 'app/components/loading/Loading';
import { publicRoutes } from 'app/routes';
import { paths } from 'app/routes/paths';
import { sendMeRequest } from 'app/store/auth/reducer';
import { useAuth } from 'app/utils/auth/useAuth';
import { useCreateUrl } from 'app/utils/router/useCreateUrl';

const AuthorizedRouter = React.lazy(() =>
  import('app/components/authorizedRouter/AuthorizedRouter'),
);

const AppRouterComponent: React.FC = () => {
  const dispatch = useDispatch();
  const createUrl = useCreateUrl();
  const { isLoading, isLoggedIn } = useAuth();
  const homeUrl = useMemo(() => createUrl(isLoggedIn ? paths.example : paths.login), [
    createUrl,
    isLoggedIn,
  ]);

  useEffect(() => {
    dispatch(sendMeRequest());
  }, [dispatch]);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <BrowserRouter basename={process.env.PUBLIC_PATH}>
      <Suspense fallback={<Loading />}>
        <Switch>
          <Redirect exact from="/" to={homeUrl} />
          {publicRoutes.map(route => (
            <Route
              key={route.id}
              path={route.path}
              component={route.component}
              exact={route.exact}
              strict={route.strict}
              sensitive={route.sensitive}
            />
          ))}
          <Route component={AuthorizedRouter} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
};

export const AppRouter = React.memo(AppRouterComponent);
