import React from 'react';
import { useTranslation } from 'react-i18next';

export const AuthorizedLayout: React.FC = ({ children }) => {
  const [t] = useTranslation('common');

  return (
    <div>
      <h1>{t('Authorized layout')}</h1>
      {children}
    </div>
  );
};
