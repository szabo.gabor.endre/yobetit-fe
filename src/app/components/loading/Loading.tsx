import React from 'react';
import { useTranslation } from 'react-i18next';

const LoadingComponent: React.FC = () => {
  const [t] = useTranslation(['common']);

  return <div>{t('common:loading')}</div>;
};

export const Loading = React.memo(LoadingComponent);
