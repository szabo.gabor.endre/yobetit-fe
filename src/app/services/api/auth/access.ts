import { Observable } from 'rxjs';

import { AjaxResponse } from 'app/utils/ajax/ajaxResponse';
import { http } from 'app/utils/ajax/http';

import type { Me } from './me';

export interface Access extends Me {
  accessToken: string;
}

export const access = (): Observable<AjaxResponse<Access>> => {
  return http.get<Access>('/auth/access');
};
