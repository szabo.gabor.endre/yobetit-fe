import { Observable } from 'rxjs';

import { AjaxResponse } from 'app/utils/ajax/ajaxResponse';
import { http } from 'app/utils/ajax/http';

export interface UserData {
  name: string;
}

export interface Me {
  userData: UserData;
}

export const me = (): Observable<AjaxResponse<Me>> => {
  return http.get<Me>('/auth/me');
};
