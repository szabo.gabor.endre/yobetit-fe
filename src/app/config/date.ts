import { enUS } from 'date-fns/locale';

import { Languages } from './languages';

export const locales = {
  [Languages.EN]: enUS,
};
