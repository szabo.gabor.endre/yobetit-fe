export enum Languages {
  EN = 'en',
}

export const defaultLanguage = Languages.EN;
export const languages = [Languages.EN];
