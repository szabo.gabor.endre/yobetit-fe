import 'core-js/stable';
import 'regenerator-runtime/runtime';
import { install as installOfflinePlugin } from 'offline-plugin/runtime';
import React from 'react';
import ReactDOM from 'react-dom';

import './i18n';
import App from './App';

ReactDOM.render(<App />, document.querySelector('#root'));

if (process.env.NODE_ENV === 'production') {
  installOfflinePlugin();
}
