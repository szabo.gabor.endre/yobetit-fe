import { Languages } from 'app/config/languages';
import { createPath } from 'app/utils/router/createPath';

export const paths = {
  example: createPath({
    [Languages.EN]: '/example',
  }),
  login: createPath({
    [Languages.EN]: '/login',
  }),
  noMatch: createPath({
    [Languages.EN]: '/404',
  }),
};
