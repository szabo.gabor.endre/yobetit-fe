import React from 'react';

import { createRoute } from 'app/utils/router/createRoute';

import { paths } from './paths';

export const publicRoutes = [
  createRoute(
    'login',
    paths.login,
    React.lazy(() => import('./login/Login')),
  ),
];

export const authorizedRoutes = [
  createRoute(
    'example',
    paths.example,
    React.lazy(() => import('./example/Example')),
  ),
];

export const noMatchRoute = createRoute(
  'noWatch',
  paths.noMatch,
  React.lazy(() => import('./noMatch/NoMatch')),
);
