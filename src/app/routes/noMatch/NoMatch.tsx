import React from 'react';
import { useTranslation } from 'react-i18next';

const NoMatch: React.FC = () => {
  const [t] = useTranslation(['common']);

  return <section>{t('common:pageNotFound')}</section>;
};

export default NoMatch;
