import React from 'react';
import { useTranslation } from 'react-i18next';

import { LanguageSelector } from 'app/components/languageSelector/LanguageSelector';
import { Link } from 'app/components/link/Link';
import { paths } from 'app/routes/paths';

import { useCounter } from './hooks/useCounter';

const Example: React.FC = () => {
  const [t] = useTranslation(['common', 'dates']);
  const [counter, increment, decrement] = useCounter();

  return (
    <>
      {t('dates:dateLong', { date: new Date() })}
      <button type="button" onClick={decrement}>
        {t('-')}
      </button>
      <h1>{counter}</h1>
      <button type="button" onClick={increment}>
        {t('+')}
      </button>
      <LanguageSelector />
      <Link path={paths.example}>{t('asdasddsa')}</Link>
    </>
  );
};

export default Example;
