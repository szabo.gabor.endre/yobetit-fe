import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { decrement, increment } from 'app/store/example/reducer';
import { getCounter } from 'app/store/example/selectors';

export const useCounter = (): [
  number,
  () => ReturnType<typeof increment>,
  () => ReturnType<typeof decrement>,
] => {
  const dispatch = useDispatch();
  const counter = useSelector(getCounter);

  const incrementAction = useCallback(() => dispatch(increment()), [dispatch]);
  const decrementAction = useCallback(() => dispatch(decrement()), [dispatch]);

  return [counter, incrementAction, decrementAction];
};
