import React, { useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { paths } from 'app/routes/paths';
import { sendLoginRequest } from 'app/store/auth/reducer';
import { loginRequestSelector } from 'app/store/auth/selectors';
import { isSuccess } from 'app/utils/ajax/loader';
import { useCreateUrl } from 'app/utils/router/useCreateUrl';

const Login: React.FC = () => {
  const [t] = useTranslation();
  const dispatch = useDispatch();
  const createUrl = useCreateUrl();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const loginRequest = useSelector(loginRequestSelector);

  const loginFormSubmit = useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      dispatch(sendLoginRequest());
    },
    [dispatch],
  );
  const handleEmailChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.currentTarget.value);
  }, []);
  const handlePasswordChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  }, []);

  if (isSuccess(loginRequest)) {
    return <Redirect to={createUrl(paths.example)} />;
  }

  return (
    <form onSubmit={loginFormSubmit}>
      <label htmlFor="email">{t('Email')}</label>
      <input type="email" value={email} id="email" onChange={handleEmailChange} />
      <label htmlFor="password">{t('Email')}</label>
      <input type="password" value={password} id="password" onChange={handlePasswordChange} />
      <button type="submit">{t('Login')}</button>
    </form>
  );
};

export default Login;
