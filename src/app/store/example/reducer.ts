import { createSlice } from '@reduxjs/toolkit';

import { ExampleState } from './types/exampleState';

const initialState: ExampleState = {
  number: 0,
};

const example = createSlice({
  initialState,
  name: 'example',
  reducers: {
    increment(state) {
      state.number += 1;
    },
    decrement(state) {
      state.number -= 1;
    },
  },
});

export const { decrement, increment } = example.actions;
export const exampleReducer = example.reducer;
