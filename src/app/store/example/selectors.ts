import type { RootState } from 'app/store/root/reducer';

import { ExampleState } from './types/exampleState';

export const getCounter = (state: RootState): ExampleState['number'] => state.example.number;
