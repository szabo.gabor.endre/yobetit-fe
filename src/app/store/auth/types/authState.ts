import type { Access } from 'app/services/api/auth/access';
import type { Me, UserData } from 'app/services/api/auth/me';
import type { Loader } from 'app/utils/ajax/loader';

export interface AuthState {
  loginRequest: Loader<Access>;
  meRequest: Loader<Me>;
  isLoggedIn: boolean;
  userData?: UserData;
}
