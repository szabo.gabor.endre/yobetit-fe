import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import type { Access } from 'app/services/api/auth/access';
import type { Me } from 'app/services/api/auth/me';
import type { AjaxResponse } from 'app/utils/ajax/ajaxResponse';
import { failure, idle, success, pending } from 'app/utils/ajax/loader';

import { AuthState } from './types/authState';

const initialState: AuthState = {
  isLoggedIn: false,
  loginRequest: idle(),
  meRequest: pending(),
};

const auth = createSlice({
  initialState,
  name: 'auth',
  reducers: {
    sendLoginRequest(state) {
      state.loginRequest = pending();
    },
    successLoginRequest(state, payload: PayloadAction<AjaxResponse<Access>>) {
      state.loginRequest = success(payload.payload);
      state.isLoggedIn = true;
      state.userData = payload.payload.response.userData;
    },
    sendMeRequest(state) {
      state.meRequest = pending();
    },
    successMeRequest(state, payload: PayloadAction<AjaxResponse<Me>>) {
      state.meRequest = success(payload.payload);
      state.isLoggedIn = true;
      state.userData = payload.payload.response.userData;
    },
    failureMeRequest(state) {
      state.isLoggedIn = false;
      state.meRequest = failure();
    },
    logout(state) {
      state.isLoggedIn = false;
      state.loginRequest = idle();
    },
  },
});

export const {
  sendLoginRequest,
  successLoginRequest,
  sendMeRequest,
  successMeRequest,
  failureMeRequest,
  logout,
} = auth.actions;
export const authReducer = auth.reducer;
