import type { RootState } from 'app/store/root/reducer';

import { AuthState } from './types/authState';

export const isLoggedInSelector = (state: RootState): AuthState['isLoggedIn'] =>
  state.auth.isLoggedIn;
export const userDataSelector = (state: RootState): AuthState['userData'] => state.auth.userData;
export const loginRequestSelector = (state: RootState): AuthState['loginRequest'] =>
  state.auth.loginRequest;
export const meRequestSelector = (state: RootState): AuthState['meRequest'] => state.auth.meRequest;
