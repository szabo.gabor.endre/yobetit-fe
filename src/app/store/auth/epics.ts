import type { Action } from 'redux';
import { combineEpics, ofType, ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, filter, map, mergeMap, takeUntil } from 'rxjs/operators';

import { access } from 'app/services/api/auth/access';
import { me } from 'app/services/api/auth/me';

import {
  sendLoginRequest,
  successLoginRequest,
  sendMeRequest,
  successMeRequest,
  failureMeRequest,
} from './reducer';

const loginRequestEpic = (action$: ActionsObservable<Action>) =>
  action$.pipe(
    filter(sendLoginRequest.match),
    mergeMap(() =>
      access().pipe(
        map(response => successLoginRequest(response)),
        takeUntil(action$.pipe(ofType(sendLoginRequest.type))),
      ),
    ),
  );

const meRequestEpic = (action$: ActionsObservable<Action>) =>
  action$.pipe(
    filter(sendMeRequest.match),
    mergeMap(() =>
      me().pipe(
        map(response => successMeRequest(response)),
        takeUntil(action$.pipe(ofType(sendMeRequest.type))),
        catchError(() => of(failureMeRequest())),
      ),
    ),
  );

export const authEpics = combineEpics(loginRequestEpic, meRequestEpic);
