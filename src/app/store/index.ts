/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { createEpicMiddleware } from 'redux-observable';
import { BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { rootEpic } from './root/epics';
import { rootReducer } from './root/reducer';

const epicMiddleware = createEpicMiddleware();
const epic$ = new BehaviorSubject(rootEpic);
const store = configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware({ thunk: false }), epicMiddleware],
});

epicMiddleware.run((...args) => epic$.pipe(switchMap(epic => epic(...args))));

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./root/reducer', () => {
    const newRootReducer = require('./root/reducer').rootReducer;

    store.replaceReducer(newRootReducer);
  });

  module.hot.accept('./root/epics', () => {
    const newRootEpic = require('./root/epics').rootEpic;

    epic$.next(newRootEpic);
  });
}

export default store;
