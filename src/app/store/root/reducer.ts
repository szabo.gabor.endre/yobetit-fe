import { combineReducers } from '@reduxjs/toolkit';

import { authReducer } from 'app/store/auth/reducer';
import { exampleReducer } from 'app/store/example/reducer';

export const rootReducer = combineReducers({
  auth: authReducer,
  example: exampleReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
