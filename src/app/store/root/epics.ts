import { combineEpics } from 'redux-observable';

import { authEpics } from 'app/store/auth/epics';

export const rootEpic = combineEpics(authEpics);
