import React, { Suspense } from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider as ReduxProvider } from 'react-redux';

import { AppRouter } from 'app/components/appRouter/AppRouter';
import { ErrorBoundary } from 'app/components/errorBoundary/ErrorBoundary';
import store from 'app/store';

const App: React.FC = () => (
  <ReduxProvider store={store}>
    <Suspense fallback={null}>
      <ErrorBoundary>
        <AppRouter />
      </ErrorBoundary>
    </Suspense>
  </ReduxProvider>
);

export default hot(App);
