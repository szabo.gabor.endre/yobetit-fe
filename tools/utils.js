const appRoot = require('app-root-path');
const loaderUtils = require('loader-utils');
const path = require('path');

function getAppRootPath() {
  return appRoot;
}

function getPath(name) {
  return getAppRootPath() + name;
}

function env(name) {
  return process.env[name];
}

function getCssLocalIdent(context, _localIdentName, localName, options) {
  // Use the filename or folder name, based on some uses the index.js / index.module.(css|scss|sass) project style
  const fileNameOrFolder = context.resourcePath.match(/index\.module\.(css|scss|sass)$/)
    ? '[folder]'
    : '[name]';

  // Create a hash based on a the file location and class name. Will be unique across a project, and close to globally unique.
  const hash = loaderUtils.getHashDigest(
    path.posix.relative(context.rootContext, context.resourcePath) + localName,
    'md5',
    'base64',
    8,
  );

  // Use loaderUtils to find the file or folder name
  const className = loaderUtils.interpolateName(
    context,
    `${fileNameOrFolder}_${localName}_${hash}`,
    options,
  );

  // remove the .module that appears in every classname when based on the file.
  return className.replace('.module_', '_');
}

module.exports = {
  getAppRootPath,
  getCssLocalIdent,
  getPath,
  env,
};
