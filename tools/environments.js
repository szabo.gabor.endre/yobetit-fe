const { getPath, env } = require('./utils');

require('dotenv').config();

module.exports = {
  paths: {
    projectRoot: getPath('/'),
    root: getPath('/src'),
    appIndex: getPath('/src/app/index.tsx'),
    build: getPath(`/${env('OUTPUT_PATH_NAME')}/`),
    public: getPath('/public/'),
    indexHtml: getPath('/public/index.ejs'),
    favicon: getPath('/public/favicon.png'),
    manifestJson: getPath('/public/manifest.json'),
    robotsTxt: getPath('/public/robots.txt'),
    packageJson: getPath('/package.json'),
  },
  devServer: {
    host: env('DEVSERVER_HOST'),
    port: env('DEVSERVER_PORT'),
    public: env('DEVSERVER_URL'),
    https: JSON.parse(env('DEVSERVER_HTTPS')),
    openBrowser: JSON.parse(env('DEVSERVER_OPEN_BROWSER')),
    proxy: {
      '/api': 'http://localhost:9000',
    },
  },
  offlinePlugin: {
    fileName: 'sw.js',
  },
  manifestJson: {
    icons: {
      folder: 'favicons',
      sizes: [96, 128, 192, 256, 384, 512, 1024],
    },
  },
  output: {
    publicPath: env('PUBLIC_PATH'),
    paths: {
      fonts: 'fonts/',
      images: 'images/',
      styles: 'styles/',
      scripts: 'scripts/',
    },
  },
  analyzer: {
    host: env('ANALYZER_HOST'),
    port: env('ANALYZER_PORT'),
  },
  apiUrl: env('API_URL'),
  date: {
    allowedLocales: ['enUS'],
  },
  jest: {
    collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/app/config/**/*'],
    coverageDirectory: env('JEST_COVERAGE_DIRECTORY'),
  },
  eslint: {
    filesCheckingPath: './src/**/*.{ts,tsx,js,jsx}',
  },
};
