const environments = require('../environments');

module.exports = {
  rootDir: environments.paths.projectRoot,
  roots: [environments.paths.root],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/tools/jest/jestPreprocess.js',
    '^.+\\.module\\.(css|scss)$': 'jest-css-modules-transform',
  },
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/tools/jest/__mocks__/fileMock.js',
    '^(?!.*?.module).*.(css|scss)$': '<rootDir>/tools/jest/__mocks__/styleMock.js',
  },
  collectCoverageFrom: environments.jest.collectCoverageFrom,
  coverageDirectory: environments.jest.coverageDirectory,
};
