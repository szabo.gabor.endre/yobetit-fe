module.exports = {
  syntax: 'postcss-scss',
  sourceMap: true,
  plugins: [require('autoprefixer')],
};
