const presets = (modules = false) => [
  [
    '@babel/preset-env',
    {
      modules,
      useBuiltIns: 'entry',
      corejs: 3,
    },
  ],
  [
    '@babel/preset-typescript',
    {
      onlyRemoveTypeImports: true,
    },
  ],
  '@babel/preset-react',
];

module.exports = {
  babelrc: false,
  presets: presets(),
  env: {
    development: {
      plugins: ['react-hot-loader/babel'],
    },
    production: {
      plugins: [
        '@babel/plugin-transform-react-constant-elements',
        [
          'transform-react-remove-prop-types',
          {
            removeImport: true,
          },
        ],
      ],
    },
  },
  overrides: [
    {
      env: {
        test: {
          presets: presets('commonjs'),
        },
      },
    },
  ],
};
