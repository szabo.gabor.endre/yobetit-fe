const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const merge = require('webpack-merge');
const WebpackPwaManifest = require('webpack-pwa-manifest');

const manifestJson = require('../../public/manifest.json');
const babelConfig = require('../babel/babel.config');
const environments = require('../environments');
const postcssConfig = require('../postcss/postcss.prod');
const { getCssLocalIdent } = require('../utils');

const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: `${environments.output.paths.scripts}[name].[contenthash:8].js`,
    chunkFilename: `${environments.output.paths.scripts}[name].[contenthash:8].chunk.js`,
    crossOriginLoading: 'anonymous',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: babelConfig,
          },
        ],
      },
      {
        test: /\.s?css$/,
        use: [
          ExtractCssChunks.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              modules: {
                auto: true,
                mode: 'pure',
                getLocalIdent: getCssLocalIdent,
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: postcssConfig,
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: environments.output.paths.images,
            },
          },
          'image-webpack-loader',
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              icon: true,
              ref: true,
            },
          },
          {
            loader: 'file-loader',
            options: {
              outputPath: environments.output.paths.images,
            },
          },
          'image-webpack-loader',
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        mode: 'write-references',
      },
      eslint: {
        enabled: true,
        files: environments.eslint.filesCheckingPath,
      },
      async: false,
    }),
    new ExtractCssChunks({
      filename: `${environments.output.paths.styles}[name].[contenthash:8].css`,
      chunkFilename: `${environments.output.paths.styles}[name].[contenthash:8].chunk.css`,
    }),
    new OfflinePlugin({
      responseStrategy: 'cache-first',
      relativePaths: false,
      publicPath: environments.output.publicPath,
      appShell: environments.output.publicPath,
      safeToUseOptionalCaches: true,
      caches: {
        main: [':rest:'],
        additional: ['*.chunk.js'],
      },
      ServiceWorker: {
        output: `${environments.output.paths.scripts}${environments.offlinePlugin.fileName}`,
        publicPath: `${environments.output.paths.scripts}${environments.offlinePlugin.fileName}`,
      },
    }),
    new WebpackPwaManifest({
      ...manifestJson,
      icons: [
        {
          src: environments.paths.favicon,
          sizes: environments.manifestJson.icons.sizes,
          destination: `${environments.output.paths.images}${environments.manifestJson.icons.folder}`,
        },
      ],
    }),
  ],
});
