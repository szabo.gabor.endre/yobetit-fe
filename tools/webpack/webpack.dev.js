const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const merge = require('webpack-merge');

const babelConfig = require('../babel/babel.config');
const environments = require('../environments');
const postcssConfig = require('../postcss/postcss.dev');
const { getCssLocalIdent } = require('../utils');

const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  output: {
    filename: `${environments.output.paths.scripts}[name].js`,
    chunkFilename: `${environments.output.paths.scripts}[name].chunk.js`,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          'thread-loader',
          'cache-loader',
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              ...babelConfig,
            },
          },
        ],
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: ExtractCssChunks.loader,
            options: {
              hmr: true,
            },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              sourceMap: true,
              modules: {
                auto: true,
                mode: 'pure',
                getLocalIdent: getCssLocalIdent,
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: postcssConfig,
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: environments.output.paths.images,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              icon: true,
              ref: true,
            },
          },
          {
            loader: 'file-loader',
            options: {
              outputPath: environments.output.paths.images,
            },
          },
        ],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    compress: true,
    noInfo: true,
    contentBase: environments.paths.build,
    host: environments.devServer.host,
    port: environments.devServer.port,
    public: environments.devServer.public,
    https: environments.devServer.https,
    proxy: environments.devServer.proxy,
    open: environments.devServer.openBrowser,
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        mode: 'write-references',
      },
      eslint: {
        enabled: true,
        files: environments.eslint.filesCheckingPath,
      },
    }),
    new ExtractCssChunks({
      filename: `${environments.output.paths.styles}[name].css`,
      chunkFilename: `${environments.output.paths.styles}[name].chunk.css`,
    }),
  ],
});
