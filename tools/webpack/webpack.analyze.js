const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const merge = require('webpack-merge');

const environments = require('../environments');

const prod = require('./webpack.prod');

module.exports = merge(prod, {
  plugins: [
    new BundleAnalyzerPlugin({
      logLevel: 'silent',
      analyzerHost: environments.analyzer.host,
      analyzerPort: environments.analyzer.port,
    }),
  ],
});
