const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');
const webpack = require('webpack');

const packageJson = require('../../package.json');
const manifestJson = require('../../public/manifest.json');
const environments = require('../environments');

module.exports = {
  stats: 'errors-warnings',
  entry: environments.paths.appIndex,
  output: {
    path: environments.paths.build,
    publicPath: environments.output.publicPath,
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: false,
    },
    runtimeChunk: {
      name: entrypoint => `runtime-${entrypoint.name}`,
    },
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', 'json'],
    modules: ['node_modules', environments.paths.root],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  module: {
    rules: [
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: environments.output.paths.fonts,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new SimpleProgressWebpackPlugin({
      format: 'minimal',
    }),
    new HtmlWebpackPlugin({
      title: manifestJson.name,
      template: environments.paths.indexHtml,
      favicon: environments.paths.favicon,
    }),
    new webpack.ContextReplacementPlugin(
      /date-fns[/\\]/,
      new RegExp(`[/\\\\](${environments.date.allowedLocales.join('|')})[/\\\\]`),
    ),
    new CopyPlugin({
      patterns: [
        environments.paths.robotsTxt,
        {
          from: `static/**/*`,
          context: environments.paths.public,
          toType: 'dir',
          noErrorOnMissing: true,
          globOptions: {
            ignore: ['.gitkeep'],
          },
        },
      ],
    }),
    new StylelintPlugin(),
    new webpack.EnvironmentPlugin({
      APP_VERSION: packageJson.version,
      PUBLIC_PATH: environments.output.publicPath,
      API_URL: environments.apiUrl,
    }),
  ],
};
