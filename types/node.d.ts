declare namespace NodeJS {
  interface ProcessEnv {
    readonly NODE_ENV: 'development' | 'production' | 'test';
    readonly API_URL: string;
    readonly APP_VERSION: string;
    readonly PUBLIC_PATH: string;
    readonly SENTRY_DSN?: string;
    readonly SENTRY_ENVIRONMENT?: string;
  }
}
